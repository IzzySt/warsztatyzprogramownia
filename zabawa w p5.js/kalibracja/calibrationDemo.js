let points = [];
let blackColor = 255;
let h = 25;
function setup() {
    createCanvas(windowWidth, windowHeight);
    //top points
    points.push(new Point(2*h, 2*h, h, blackColor)); 
    points.push(new Point(windowWidth/2, 2*h, h, blackColor)); 
    points.push(new Point(windowWidth - 2*h, 2*h, h, blackColor)); 
    points.push(new Point(2*h, windowHeight - 2*h,h, blackColor)); 
    points.push(new Point(windowWidth/2,windowHeight - 2*h, h, blackColor)); 
    points.push(new Point(windowWidth - 2*h, windowHeight - 2*h, h, blackColor)); 
    //bottom points
    points.push(new Point(2*h, windowHeight/2, h, blackColor)); 
    points.push(new Point(windowWidth - 2*h, windowHeight/2, h, blackColor)); 
    points.push(new Point(windowWidth/2, windowHeight/2, h, blackColor)); 
}

function draw() {    
    background(168, 129, 50);
    points.forEach(function(point){
        point.show();
    });
}

function createCalibrationPoint(colorGradient, x, y, h)
{
    fill(colorGradient);
    ellipse(x,y,h);
}

function mousePressed()
{
    points.forEach(function(point){
        let distance = dist(mouseX, mouseY, point.getX(), point.getY());
        if(distance < h)
        {
            point.changeColor(50);
        } 
    });
}