

function setup() {
    createCanvas(700, 500);
    background(168, 129, 50);
}

var x = 450;
var y = 200;

function draw() {    
    // rysowanie twarzy
    background(168, 129, 50);
    fill(51, 49, 41);
    //rect(x, y, szer, wys, top-L, top-R, bottom-R, bottom-L)
    rect(150, 150, 120, 100, 20, 20, 10, 10); // ostatnie parametr zaokrąglają rogi 
    fill(230, 215, 163);
    rect(155, 195, 110, 55, 5, 5, 15,15);

    // rysowanie oczu
    //ellipse(x, y, szer, [wys])
    fill('white');
    ellipse(170, 180, 25);
    ellipse(245, 180, 25);

    //rysowanie uszu
    fill(51, 49, 41);
    triangle(167, 150, 185, 150, 173, 130);
    triangle(255, 150, 230, 150, 243, 130);

    //rysowanie nietoperka 1
    rect(350, 120, 50, 25);
    rect(325, 95, 25, 25);
    rect(400, 95, 25, 25);

    //rysowanie nietoperka 2
    rect(450, 200, 50, 25);
    rect(425, 175, 25, 25);
    rect(500, 175, 25, 25);

    //rysowanie nietoperka który podąża za myszką xD
    rect(mouseX, mouseY, 50, 25);
    rect(mouseX - 25, mouseY-25, 25, 25);
    rect(mouseX + 50, mouseY-25, 25, 25);

    //rysowanie nietoperka który ma potrzebę ruchu xD

    rect(x, y, 50, 25);
    rect(x - 25, y - 25, 25, 25);
    rect(x + 50, y - 25, 25, 25);
    x= x + 3;
    y= y + 3;
}