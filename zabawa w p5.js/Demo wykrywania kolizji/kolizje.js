function setup()
{
    createCanvas(windowWidth, windowHeight);
    ellipseMode(CENTER);
}

let h = 50;
let xRed= 250;
let yRed = 150;
let xGreen = 400;
let yGreen = 200;
let d=500;

function draw()
{
    if(GameOver(d, h)) 
    {
        text("game OVER!", 350, 350);
        return;
    }
    background("gray");
    fill("green");
    // wzór na obliczanie odległości między dwoma punktami
    d = sqrt(pow((xRed - mouseX), 2) + pow((yRed - mouseY), 2));
    text(d, 100, 100);
    ellipse(mouseX, mouseY, h);

    fill("yellow");
    //krok 1 - <=  to to samo co d<h || d == h
    //krok 2 h jest równie dwóm promieniom czyli r1 + r2 = h1/2 + h2/2 = h
    if(d <= h )
    {
        fill("red");
    }  
    ellipse(xRed, yRed, h);

}

function GameOver(d, h)
{
    if(d <= h)
        return true;
    return false;
}

