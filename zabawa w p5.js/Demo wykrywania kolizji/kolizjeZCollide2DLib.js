function setup()
{
    createCanvas(600, 600);    
    ellipseMode(CENTER);
}

let h = 50;
let xYellow = 100;
let yYellow = 100;
let hit = false;

function draw()
{
    if(hit == true)
    {
        text("game OVER!", 250, 250);
        fill("red");
        ellipse(xYellow, yYellow, h);
        return;
    }
    background("gray");

    fill("yellow");
    ellipse(xYellow, yYellow, h);

    fill("green");
    ellipse(mouseX, mouseY, h);

    hit = collideCircleCircle(xYellow, yYellow, h, mouseX, mouseY, h);
}