let wysokosc = 300;
let szerokosc = 600;
let gruboscLinii = 3;
let gruboscPunktu = 15;
let oIlePrzesunacLinie = 20;

let iloscKlatek = 30;

function setup() {
  createCanvas(szerokosc, wysokosc);
  frameRate(iloscKlatek);
  
  // background("indianRed");
}

let liniaPoczatekX = 10;
let liniaPoczatekY = 10;

let liniaKoniecX = 10;
let liniaKoniecY = wysokosc - 10;

function draw() {
  background("indianRed");

  debugger;
  
  stroke("lime");
  strokeWeight(gruboscPunktu);
  point(liniaPoczatekX, liniaPoczatekY);
  
  stroke("papayawhip");
  point(liniaKoniecX, liniaKoniecY);
  
  stroke("white");
  strokeWeight(gruboscLinii);
  line(liniaPoczatekX, liniaPoczatekY, liniaKoniecX, liniaKoniecY);
  
  liniaPoczatekX = liniaPoczatekX + oIlePrzesunacLinie;
  liniaKoniecX = liniaKoniecX + oIlePrzesunacLinie;
  text(liniaPoczatekX, 100, 100);
}
