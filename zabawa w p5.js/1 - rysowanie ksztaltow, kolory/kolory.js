function setup(){
    createCanvas(100, 100);
  }
  function draw(){
    point(40, 50); // point(x, y)
    rect(10, 20, 40, 30); // rect(x, y, width, height)
    ellipse(30, 30, 40, 60); // ellipse(x, y, width, height)
    line(90,150,80,160);
  }