1- teoria:
https://p5js.org/learn/color.html

kolory jako coś co komputer rozumie w postaci liczb (dążymy do zer i jedynek)
Model barw RGB, color picker: https://www.google.com/search?client=firefox-b-d&q=color+picker


  
background(255);    // Setting the background to white
stroke(0);          // Setting the outline (stroke) to black
fill(150);          // Setting the interior of a shape (fill) to grey
rect(50,50,75,100); // Drawing the rectangle

function draw() {
  background(255);
  noStroke();

  // Bright red
  fill(255,0,0);
  ellipse(20,20,16,16);

  // Dark red
  fill(127,0,0);
  ellipse(40,20,16,16);

  // Pink (pale red)
  fill(255,200,200);
  ellipse(60,20,16,16);
}