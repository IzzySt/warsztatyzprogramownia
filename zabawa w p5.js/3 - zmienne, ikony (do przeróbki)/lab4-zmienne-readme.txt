1 - teoria:
https://kursjs.pl/kurs/super-podstawy/zmienne.php
-> podrozdziały do przeczytania: 
  -Deklarowanie zmiennych
  -Po co stosować zmienne?
  -Nazewnictwo zmiennych
  -Różnice między var a let/const * - opcjonalnie
  -Podsumowanie
  
 Zadanie na zajęciach:
  - teoria dot. osi X, osi Y, co to canvas? (patrz wyjaśnienie CodingTrain)
  - narysuj prostokąt w p5.js
  - spróbuj wprowadzić zmienne - co może w przypadku prostokątu się zmieniać?
    - przemieszczanie porstokątu góra / dół / skos 1 / skos 2
    - powiększanie / pomniejszanie prostokąta 
    - jak można te umiejętności zastosować w praktyce w grach?