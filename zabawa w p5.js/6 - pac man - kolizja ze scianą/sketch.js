function setup() {
    createCanvas(700, 450);    
}

let grayColor = '#c7bda9';
let orangeColor = '#ffc654';
let yellowColor = '#ffee54';
let wallHeight = 50;
let wallWidth = 200;
let bazeSize = 50;
let pacmanX = 20;
let pacmanY = 20;
let pacmanH = 30;
let isHitWall = false;

function draw() {
    background(grayColor);
    
    //pacman
    noStroke();
    fill(yellowColor);
    ellipse(pacmanX, pacmanY, pacmanH);

    fill(orangeColor);
    stroke('red');

    // rect(X, Y, wysokosc, szerokosc)
    //filary
    rect(bazeSize, 2*bazeSize, wallHeight, wallWidth);
    isHitWall = collideRectCircle(bazeSize, 2*bazeSize, wallHeight, wallWidth,pacmanX, pacmanY, pacmanH);
    
    rect(3*bazeSize, 2*bazeSize, wallHeight, wallWidth);
    isHitWall = collideRectCircle(3*bazeSize, 2*bazeSize, wallHeight, wallWidth, pacmanX, pacmanY, pacmanH);

    rect(10* bazeSize, 2*bazeSize, wallHeight, wallWidth);
    isHitWall = collideRectCircle(10* bazeSize, 2*bazeSize, wallHeight, wallWidth, pacmanX, pacmanY, pacmanH);

    rect(12*bazeSize, 2*bazeSize, wallHeight, wallWidth);
    isHitWall = collideRectCircle(12*bazeSize, 2*bazeSize, wallHeight, wallWidth, pacmanX, pacmanY, pacmanH);

    //poprzeczne ściany
    rect(5*bazeSize, bazeSize, wallWidth, wallHeight);
    isHitWall = collideRectCircle(5*bazeSize, bazeSize, wallWidth, wallHeight, pacmanX, pacmanY, pacmanH);

    rect(5*bazeSize, 3*bazeSize + wallWidth, wallWidth, wallHeight);
    isHitWall = collideRectCircle(5*bazeSize, 3*bazeSize + wallWidth, wallWidth, wallHeight, pacmanX, pacmanY, pacmanH);

    //box in the center
    rect(5*bazeSize, 3*bazeSize, wallHeight /2, wallWidth /2);
    isHitWall = collideRectCircle(5*bazeSize, 3*bazeSize, wallHeight /2, wallWidth /2, pacmanX, pacmanY, pacmanH);

    rect(4*bazeSize + wallWidth, 3*bazeSize, wallHeight /2, wallWidth /2);
    isHitWall = collideRectCircle(4*bazeSize + wallWidth, 3*bazeSize, wallHeight /2, wallWidth /2, pacmanX, pacmanY, pacmanH);

    rect(5*bazeSize + bazeSize/2, 5*bazeSize,  3*bazeSize - 25, bazeSize/ 2);
   // isHitWall = collideRectCircle(5*bazeSize + bazeSize/2, 5*bazeSize,  3*bazeSize - 25, bazeSize/ 2, pacmanX, pacmanY, pacmanH);

    textSize(16);
    text(isHitWall, 100, 100);
}

let step = 15;

function keyPressed()
{
    if(keyCode == LEFT_ARROW)
    {
        pacmanX -=step;
    }
    else if(keyIsDown(RIGHT_ARROW))
    {
        pacmanX+=step;
    }
    else if(keyCode == UP_ARROW)
    {
        pacmanY-=step;
    }
    else if(keyCode == DOWN_ARROW)
    {
        pacmanY+=step;
    }
}