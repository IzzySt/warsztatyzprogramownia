function setup() {
    createCanvas(700, 450);
    
}
let grayColor = '#c7bda9';
let orangeColor = '#ffc654';
let yellowColor = '#ffee54';
let wallHeight = 50;
let wallWidth = 200;
let bazeSize = 50;
let pacmanX = 20;
let pacmanY = 20;

function draw() {
    background(grayColor);
    fill(orangeColor);
    stroke('red');

    // rect(X, Y, wysokosc, szerokosc)
    //filary
    rect(bazeSize, 2*bazeSize, wallHeight, wallWidth);
    rect(3*bazeSize, 2*bazeSize, wallHeight, wallWidth);
    rect(10* bazeSize, 2*bazeSize, wallHeight, wallWidth);
    rect(12*bazeSize, 2*bazeSize, wallHeight, wallWidth);

    //poprzeczne ściany
    rect(5*bazeSize, bazeSize, wallWidth, wallHeight);
    rect(5*bazeSize, 3*bazeSize + wallWidth, wallWidth, wallHeight);

    //box in the center
    rect(5*bazeSize, 3*bazeSize, wallHeight /2, wallWidth /2);
    rect(4*bazeSize + wallWidth, 3*bazeSize, wallHeight /2, wallWidth /2);
    rect(5*bazeSize + bazeSize/2, 5*bazeSize,  3*bazeSize - 25, bazeSize/ 2);

    noStroke();
    fill(yellowColor);
    ellipse(pacmanX, pacmanY, 40);

}

let step = 10;
function keyPressed()
{

    if(keyCode == LEFT_ARROW)
    {
        pacmanX -=step;
    }
    else if(keyIsDown(RIGHT_ARROW))
    {
        pacmanX+=step;
    }
    else if(keyCode == UP_ARROW)
    {
        pacmanY-=step;
    }
    else if(keyCode == DOWN_ARROW)
    {
        pacmanY+=step;
    }
}